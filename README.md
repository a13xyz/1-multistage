# 1-multistage
# Мультистейдж сборка докер

В base - nginx на основе дебиан 9 
В мультистейдж - докерфайл для того же образа, только облегченная версия


systemctl start docker # запускаю докер
sudo docker build -t nginxsc:v1 ./base/ #собираю образ nginx со стандартными модулями
sudo docker build -t nginxsc:v2 -f ./multistage/ #собираю мультиобраз

sudo docker images # убеждаюсь, что они созданы
sudo docker run -v "$(pwd)"/nginx.conf:/home/nginx.conf --name=nginx_for_less4_v3 -d -p 8003:80 nginx:latest # передаю в контейнер конфиг файл. Взял в скобки, потому что без них — ошибка docker: invalid reference format
sudo docker exec -ti nginx_for_less4_v3 cat /home/nginx.conf # открываю в режиме отладки конфиг файл в контейнере и вижу содержимое nginx.con
Открываю в браузере http://localhost:8003/ — вижу «Welcome to nginx!»


#sudo docker build -t nginxsc:v1 . #собираю образ nginx со стандартными модулями
#sudo docker build -t nginxsc:v2 -f Dockerfilem . #собираю мультиобраз